'use strict'
require('dotenv').config()
const mongoose = require('mongoose');
const app = require('../Server/index');
const port = process.env.APP_PORT;

//Generar promesa global
mongoose.Promise = global.Promise;

//Conexion a la db
mongoose.connect('mongodb://127.0.0.1:27017/refaccionariaproyec', { useNewUrlParser: true })
    .then(() => {
        console.log('Base de datos corriendo');

        //Escuchar puerto del server
        app.listen(port, () => {
            console.log( `Server corriendo en puerto: ${port}`);
        });
    });

