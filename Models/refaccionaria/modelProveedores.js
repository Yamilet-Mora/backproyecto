const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoProveedor = new Schema({
    nombreProveedor: { type: String },
    telefonoProveedor: { type: Number },
    correoProveedor: { type: String },
    direccionProveedor: { type: String },
})

module.exports = mongoose.model('nuevoProveedor', nuevoProveedor);