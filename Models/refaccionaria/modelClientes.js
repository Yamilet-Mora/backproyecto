const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoCliente = new Schema({
    nombreCliente: { type: String },
    telefonoCliente: { type: Number },
    direccionCliente: { type: String },
})

module.exports = mongoose.model('nuevoCliente', nuevoCliente);