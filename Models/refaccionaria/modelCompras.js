const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevaCompra = new Schema({
    proveedor: { type: String },
    productoCompra: { type: String },
    cantidadCompra: { type: Number },
    total: { type: Number },
    fecha: { type: String },
})

module.exports = mongoose.model('nuevaCompra', nuevaCompra);