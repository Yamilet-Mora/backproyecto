const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const { appConfig } = require('../../config')

let nuevoProducto = new Schema({
    nombreProducto: { type: String },
    marcaProducto: { type: String },
    presentacionProducto: { type: String },
    contenidoProducto: { type: String },
    precioProducto: { type: String },
    descripcionProducto: { type: String },
    proovedor:{type:String}
});

module.exports = mongoose.model('nuevoProducto', nuevoProducto);