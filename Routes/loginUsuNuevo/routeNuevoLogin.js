const express = require('express');
const modelUsu = require('../../Models/refaccionaria/modelLoginUsu');
const bcrypt = require('bcrypt');


let app = express();

app.post('/usu/nuevo' , async (req,res) => {
    let body = req.body;
    console.log(body);

    let newSchemaUsu = new modelUsu ({
        nombreUsu: body.nombreUsu,
        passwordUsu: await bcrypt.hash(body.passwordUsu, 10),
        roleUsu: body.roleUsu,
    });

    newSchemaUsu
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Datos no guardados',
                        err
                    });
            }
        );
});

app.get('/obtener/usuario', async (req, res) => {
    const respuesta = await modelUsu.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/usuario/:id' , async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelUsu.findById(id);
    res.status(200)
        .json({
            ok: true,
            respuesta
        });
});

app.delete('/borrar/usuario/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelUsu.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/usuario/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelUsu.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;