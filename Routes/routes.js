const express = require('express');
const app = express();

app.use(require('./test'));
app.use(require('./productoNuevo/routeNuevoProducto'));
app.use(require('./proveedorNuevo/routeNuevoProveedor'));
app.use(require('./clienteNuevo/routeNuevoCliente'));
app.use(require('./loginAdmiNuevo/routeNuevoLoginAdmi'));
app.use(require('./loginUsuNuevo/routeNuevoLoginUsu'));
app.use(require('./ventaNueva/routeNuevaVenta'));

module.exports = app;