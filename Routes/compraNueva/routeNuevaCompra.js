const express = require('express');
const modelCompra = require('../../Models/refaccionaria/modelCompras');


let app = express();

app.post('/compra/nueva' , (req,res) => {
    let body = req.body;
    console.log(body);

    let newSchemaCompra = new modelCompra ({
        proveedor: body.proveedor,
        productoCompra: body.productoCompra,
        cantidadCompra: body.cantidadCompra,
        total: body.total,
        fecha: body.fecha,
    });

    newSchemaCompra
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Datos no guardados',
                        err
                    });
            }
        );
});

app.get('/obtener/compra', async (req, res) => {
    const respuesta = await modelCompra.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/compra/:id' , async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelCompra.findById(id);
    res.status(200)
        .json({
            ok: true,
            respuesta
        });
});

app.delete('/borrar/compra/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelCompra.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/compra/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelCompra.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;