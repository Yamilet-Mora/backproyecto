const express = require('express');
const modelProveedor = require('../../Models/refaccionaria/modelProveedores');

let app = express();

app.post('/proveedor/nuevo' , (req,res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProveedor = new modelProveedor ({
        nombreProveedor: body.nombreProveedor,
        telefonoProveedor: body.telefonoProveedor,
        correoProveedor: body.correoProveedor,
        direccionProveedor: body.direccionProveedor,
    });

    newSchemaProveedor
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Datos no guardados',
                        err
                    });
            }
        );
});

app.get('/obtener/proveedor', async (req, res) => {
    const respuesta = await modelProveedor.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/proveedor/:id' , async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelProveedor.findById(id);
    res.status(200)
        .json({
            ok: true,
            respuesta
        });
});

app.delete('/borrar/proveedor/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelProveedor.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/proveedor/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelProveedor.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;