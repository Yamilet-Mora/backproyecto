const express = require('express');
const modelCliente = require('../../Models/refaccionaria/modelClientes');

let app = express();

app.post('/cliente/nuevo' , (req,res) => {
    let body = req.body;
    console.log(body);

    let newSchemaCliente = new modelCliente ({
        nombreCliente: body.nombreCliente,
        telefonoCliente: body.telefonoCliente,
        direccionCliente: body.direccionCliente,
    });

    newSchemaCliente
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Datos no guardados',
                        err
                    });
            }
        );
});

app.get('/obtener/cliente', async (req, res) => {
    const respuesta = await modelCliente.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/cliente/:id' , async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelCliente.findById(id);
    res.status(200)
        .json({
            ok: true,
            respuesta
        });
});

app.delete('/borrar/cliente/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelCliente.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/cliente/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelCliente.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;