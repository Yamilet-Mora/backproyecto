const express = require('express');
const modelVenta = require('../../Models/refaccionaria/modelVentas');


let app = express();

app.post('/venta/nueva' , (req,res) => {
    let body = req.body;
    console.log(body);

    let newSchemaVenta = new modelVenta ({
        cliente:body.cliente,
        productoVenta: body.productoVenta,
        subtotal: body.subtotal,
        iva: body.iva,
        total: body.total,
    });

    newSchemaVenta
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Datos no guardados',
                        err
                    });
            }
        );
});

app.get('/obtener/venta', async (req, res) => {
    const respuesta = await modelVenta.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/venta/:id' , async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelVenta.findById(id);
    res.status(200)
        .json({
            ok: true,
            respuesta
        });
});

app.delete('/borrar/venta/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelVenta.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/venta/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelVenta.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;