const express = require('express');
const modelAdmi = require('../../Models/refaccionaria/modelLoginAdmi');
const bcrypt = require('bcrypt');


let app = express();

app.post('/administrador/nuevo' , async (req,res) => {
    let body = req.body;
    console.log(body);

    let newSchemaAdmi = new modelAdmi ({
        nombreAdm: body.nombreAdm,
        passwordAdm: body.passwordAdm,
    });

    newSchemaAdmi
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Datos no guardados',
                        err
                    });
            }
        );
});

app.get('/obtener/administrador', async (req, res) => {
    const respuesta = await modelAdmi.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/administrador/:id' , async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelAdmi.findById(id);
    res.status(200)
        .json({
            ok: true,
            respuesta
        });
});
app.get('/buscar/usuario/correo/:nombreAdm', async(req, res) => {
    let nombre = req.params.nombreAdm;
    const DatosInicioSesion =  await modelAdmi.findOne({ nombreAdm: nombre });

    res.status(200).json({
        ok : true,
        DatosInicioSesion
    });
});
app.delete('/borrar/administrador/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelAdmi.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/administrador/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelAdmi.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});

module.exports = app;