const express = require('express');
const modelProducto = require('../../Models/refaccionaria/modelProductos');
const upload = require ('../../Lib/storage')

let app = express();

app.post('/producto/nuevo', upload.single('imagenProducto') ,(req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProducto = new modelProducto({
        nombreProducto: body.nombreProducto,
        marcaProducto: body.marcaProducto,
        presentacionProducto: body.presentacionProducto,
        contenidoProducto: body.contenidoProducto,
        precioProducto: body.precioProducto,
        descripcionProducto: body.descripcionProducto,
        proovedor:body.proovedor
    });

    newSchemaProducto
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Datos no guardados',
                        err
                    });
            }
        );
});


app.get('/obtener/producto', async (req, res) => {
    const respuesta = await modelProducto.find()
    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/producto/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelProducto.findById(id);
    res.status(200)
        .json({
            ok: true,
            respuesta
        });
});

app.delete('/borrar/producto/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelProducto.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});

app.put('/update/producto/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelProducto.findByIdAndUpdate(id, campos, { new: true });
    res.status(202).json({
        ok: true,
        msj: "DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});



module.exports = app;